//
//  AppDelegate.m
//  NIXProject
//
//  Created by Egor Zubkov on 1/22/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//

#if (defined(CONFIGURATION_Debug))
    #import <SDStatusBarManager.h>
#endif

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TwitterKit/TwitterKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window = window_;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[[Crashlytics class], [Twitter class]]];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
#if (defined(CONFIGURATION_Debug))
    
    #if TARGET_IPHONE_SIMULATOR
    {
        [[SDStatusBarManager sharedInstance] enableOverrides];
    }
    #endif
#endif
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[Twitter sharedInstance] application:application openURL:url options:nil])
    {
        return YES;
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

@end

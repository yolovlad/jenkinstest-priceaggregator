//
//  ItemsController.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 10/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "ItemsRepositoryProtocol.h"
#import "ShopRepository.h"
#import "ShopsRepositoryProtocol.h"
#import "YMLParser.h"
#import <Foundation/Foundation.h>
#import "ItemsControllerProtocol.h"
#import "ShopsControllerProtocol.h"

@interface StorageController : NSObject<ItemsControllerProtocol, ShopsControllerProtocol>

@end

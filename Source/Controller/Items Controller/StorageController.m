//
//  ItemsController.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 10/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "ItemsRepositoryProtocol.h"
#import "ShopQueryManagerProtocol.h"
#import "YMLParser.h"
#import "ShopRepository.h"
#import "ItemsRepositoryProtocol.h"
#import "StorageController.h"
@interface StorageController ()

@property(weak, nonatomic) id<ItemsRepositoryProtocol> itemsRepository;
@property(weak, nonatomic) id<ShopsRepositoryProtocol> shopsRepository;

@property(strong, nonatomic) YMLParser *parser;
@property(strong, nonatomic) ItemsRepository *repositoryAnchor;
@property(strong, nonatomic) ShopRepository *storeManagerAnchor;

@end
@implementation StorageController

- (instancetype)init
{
    self = [super init];
    [self setRepositoryAnchor:[[ItemsRepository alloc] initWithStorageController:self]];
    [self setItemsRepository:[self repositoryAnchor]];
    [self setStoreManagerAnchor:[[ShopRepository alloc] initWithStorageController:self]];
    [self setShopsRepository:[self storeManagerAnchor]];
    [self setParser:[YMLParser new]];
    return self;
}

#pragma ShopControllerProtocol
- (void)addShop:(NSString *)shopURL :(void (^)(NSArray *))block
{
    [[self shopsRepository] addShopToStorage:shopURL:^(NSArray *storeList)
    {
        block(storeList);
    }];
}

- (void)deleteShop:(NSString *)shopName
{
    [[self shopsRepository] deleteShopFromStorage:shopName];
    [[self itemsRepository] removeItemsFromStorageForShop:shopName];
}

- (void)addFavoriteItem:(NSString *)offerId ToShop:(NSString *)shopName
{
    [[self shopsRepository] addFavoriteItem:offerId ToShop:shopName];
}

- (void)deleteFavoriteItem:(NSString *)offerId FromShop:(NSString *)shopName
{
    [[self shopsRepository] deleteFavoriteItem:offerId FromShop:shopName];
}

- (NSArray *)getShopList
{
    return [[self shopsRepository] getShopList];
}

#pragma ItemControllerProtocol
- (void)getFavoriteItemList:(void (^)(NSArray *))block;
{
    NSMutableArray *finalArray = [NSMutableArray new];
    NSMutableArray *shopArray = [[[self shopsRepository] getShopList] mutableCopy];
    [[self itemsRepository] getAllItems:^(NSArray *itemsArray)
    {
        for (int shop = 0; shop < shopArray.count; shop++)
        {
            NSMutableArray *arrayOfResult = [[shopArray objectAtIndex:shop] objectForKey:@"favorites"];
            
            for (int favoritelist = 0; favoritelist < arrayOfResult.count; favoritelist++)
            {
                for (int i = 0; i < itemsArray.count; i++)
                {
                    if ([[[itemsArray objectAtIndex:i] offerId] isEqualToString:[[[shopArray objectAtIndex:shop] objectForKey:@"favorites"] objectAtIndex:favoritelist]])
                    {
                        [[itemsArray objectAtIndex:i] setFavorite:YES];
                        [finalArray addObject:[itemsArray objectAtIndex:i]];
                    }
                }
            }
        }
        
        block(finalArray);
    }];
}

- (void)addShopAndItemsToStorage:(NSString *)shopURL :(void (^)(NSString *, NSError *))didAddingShop
{
    [[self itemsRepository] addShopAndItemsToStorage:shopURL:^(NSString *shopName, NSError *error)
    {
        didAddingShop(shopName, error);
    }];
}

- (void)getItemListForRequest:(NSString *)request WithFilters:(Filter *)filter :(void (^)(NSArray *))block
{
    [[self itemsRepository] getItemsFromStorageForRequest:request WithFilter:filter:^(NSArray *result)
    {
        block(result);
    }];
}

@end

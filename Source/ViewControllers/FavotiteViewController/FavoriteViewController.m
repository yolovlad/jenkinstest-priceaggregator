//
//  FavoriteViewController.m
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import <Accounts/AccountsDefines.h>
#import "FavoriteViewController.h"
#import <Accounts/Accounts.h>
@interface FavoriteViewController ()

@property(weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property(strong, nonatomic) NSMutableArray *favoriteItemsList;

@property(weak, nonatomic) id<ItemQueryManagerProtocol> itemRequestManager;
@property(strong, nonatomic) ItemQueryManager *itemRequestManagerAnchor;

@property(strong, nonatomic) ShopQueryManager *shopsRequestManagerAnchor;
@property(weak, nonatomic) id<ShopQueryManagerProtocol> shopsManager;

@end
@implementation FavoriteViewController
{
    NSURL *urlForOffer;
    ACAccountStore *myStore;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setItemRequestManagerAnchor:[ItemQueryManager new]];
    [self setItemRequestManager:[self itemRequestManagerAnchor]];
    [self setShopsRequestManagerAnchor:[ShopQueryManager new]];
    [self setShopsManager:[self shopsRequestManagerAnchor]];
   
    [self addRevealViewController];
    [[self collectionView] registerNib:[UINib nibWithNibName:@"OfferCell" bundle:nil] forCellWithReuseIdentifier:@"OfferCell"];
    [[self itemRequestManager] getFavoriteItemList:^(NSArray *favoriteItems)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self setFavoriteItemsList:[favoriteItems mutableCopy]];
            [[self collectionView] reloadData];
        });
    }];
}

- (void)addRevealViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController)
    {
        [[self sidebarButton] setTarget:self.revealViewController];
        [[self sidebarButton] setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
    
    [revealViewController setRightViewController:nil];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OfferCell *cell = [[self collectionView] dequeueReusableCellWithReuseIdentifier:@"OfferCell" forIndexPath:indexPath];
    [cell setupCellWithOffer:[[self favoriteItemsList] objectAtIndex:indexPath.row]];
    [cell setDelegate:self];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self favoriteItemsList] count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma Offer Action Delegate
- (void)deleteCellFromFavorite:(OfferCell *)cell
{
    [self createDeletingAlertForFavoriteCell:cell];
}

- (void)shareCell:(OfferCell *)cell
{
    [self creatingSharingAlertWithCellData:cell];
}

- (void)openURL:(NSURL *)url
{
    urlForOffer = url;
    [self performSegueWithIdentifier:@"segueToBrowser" sender:self];
}

#pragma creating allertViews
- (void)createDeletingAlertForFavoriteCell:(OfferCell *)cell
{
    NSString *cancel = NSLocalizedString(@"alert.cancel", nil);
    NSString *comfirm = NSLocalizedString(@"alert.confirm", nil);
    NSString *title = NSLocalizedString(@"alert.title.deleting_from_favorite.title", nil);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:comfirm
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action)
                                                            {
                                                                [[self collectionView] performBatchUpdates:^
                                                                {
                                                                    NSIndexPath *indexPath = [[self collectionView] indexPathForCell:cell];
                                                                    [[self shopsManager] deleteFavoriteItem:[cell identifier] FromShop:[cell shopName]];
                                                                    [[self itemRequestManager] getFavoriteItemList:^(NSArray *favoriteItems)
                                                                    {
                                                                        dispatch_async(dispatch_get_main_queue(), ^
                                                                        {
                                                                            [self setFavoriteItemsList:[favoriteItems mutableCopy]];
                                                                            [[self collectionView] deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
                                                                        });
                                                                    }];
                                                                } completion:^(BOOL finished){}];
                                                            }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancel
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {}];
    [alert addAction:cancelAction];
    [alert addAction:submitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)creatingSharingAlertWithCellData:(OfferCell *)cell
{
    NSString *facebook = NSLocalizedString(@"alert.action.facebook", nil);
    NSString *twitter = NSLocalizedString(@"alert.action.twitter", nil);
    NSString *instagram = NSLocalizedString(@"alert.action.instagram", nil);
    NSString *cancel = NSLocalizedString(@"alert.cancel", nil);
    NSString *shareTitle = NSLocalizedString(@"alert.share.title", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:shareTitle
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *facebookAction = [UIAlertAction actionWithTitle:facebook
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action){}];
    UIAlertAction *twitterAction = [UIAlertAction actionWithTitle:twitter
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action){}];
    UIAlertAction *instagramAction = [UIAlertAction actionWithTitle:instagram
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action){}];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancel
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action) {}];
    [alert addAction:instagramAction];
    [alert addAction:facebookAction];
    [alert addAction:twitterAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - CollectionFlowLayoutDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView
                 layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(self.collectionView.bounds.size.width - 40, 240);
    return size;
}

#pragma prapare for segue methods
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueToBrowser"])
    {
        BrowserViewController *browserViewController = [segue destinationViewController];
        [browserViewController setCurrentURL:urlForOffer];
    }
}

@end

//
//  FavoriteViewController.h
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "OfferCell.h"
#import "ShopQueryManager.h"
#import "ShopQueryManagerProtocol.h"
#import <UIKit/UIKit.h>
#import "ItemQueryManager.h"
#import "BrowserViewController.h"
#import "ItemQueryManagerProtocol.h"
#import "OfferActionProtocol.h"
#import "SWRevealViewController.h"

@interface FavoriteViewController : UICollectionViewController<OfferActionProtocol, UICollectionViewDelegateFlowLayout>
@end

//
//  BrowserVoewController.h
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import <WebKit/WebKit.h>
#import <UIKit/UIKit.h>

@interface BrowserViewController : UIViewController<WKNavigationDelegate>

@property(strong, nonatomic) NSURL *currentURL;

@end

//
//  BrowserVoewController.m
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "BrowserViewController.h"
@interface BrowserViewController ()<WKNavigationDelegate>

@property(weak, nonatomic) IBOutlet WKWebView *webView;

@end

@implementation BrowserViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL *url = [self currentURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [[self webView] loadRequest:request];
}

@end

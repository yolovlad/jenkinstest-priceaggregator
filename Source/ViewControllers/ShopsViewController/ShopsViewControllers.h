//
//  ShopsViewControllers.h
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "ShopCell.h"
#import <UIKit/UIKit.h>
#import "ShopRepository.h"
#import "ShopQueryManager.h"
#import "ShopQueryManagerProtocol.h"
#import "SWRevealViewController.h"
@interface ShopsViewControllers : UITableViewController

@end

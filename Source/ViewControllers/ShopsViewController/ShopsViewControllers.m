
#import "ShopsViewControllers.h"
@interface ShopsViewControllers ()<UIGestureRecognizerDelegate>

@property(strong, nonatomic) NSMutableArray *shopsList;
@property(strong, nonatomic) ShopQueryManager *shopsRequestManagerAnchor;
@property(weak, nonatomic) id<ShopQueryManagerProtocol> shopsManager;
@property(weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

- (void)addRevealViewController;
- (void)deleteShop:(NSString *)shopName;

@end

@implementation ShopsViewControllers
{
    NSIndexPath *indexPathForDeleting;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addRevealViewController];
    [self setShopsRequestManagerAnchor:[ShopQueryManager new]];
    [self setShopsManager:[self shopsRequestManagerAnchor]];
    [self setShopsList:[[[self shopsManager] getShopList] mutableCopy]];
    
    UINib *filterSliderCell = [UINib nibWithNibName:@"ShopCell" bundle:nil];
    [self.tableView registerNib:filterSliderCell forCellReuseIdentifier:@"shopCell"];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPress.minimumPressDuration = 0.5;
    longPress.delegate = self;
    [[self tableView] addGestureRecognizer:longPress];
}

- (void)addRevealViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController)
    {
        [[self sidebarButton] setTarget:self.revealViewController];
        [[self sidebarButton] setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
    
    [revealViewController setRightViewController:nil];
}

#pragma tableView DataSource/Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopCell" forIndexPath:indexPath];
    [cell setupCellWithShop:[[self shopsList] objectAtIndex:indexPath.row]];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self shopsList] count];
}

#pragma deleting/adding cells
- (void)deleteShop:(NSString *)shopName;
{
    [[self shopsManager] deleteShop:shopName];
    [self setShopsList:[[[self shopsManager] getShopList] mutableCopy]];
    [[self tableView] deleteRowsAtIndexPaths:@[indexPathForDeleting] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)addShopWithUrl:(NSString *)url
{
    [[self shopsManager] addShop:url:^(NSArray *array)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self setShopsList:[[[self shopsManager] getShopList] mutableCopy]];
            [[self tableView] reloadData];
        });
    }];
    [self setShopsList:[[[self shopsManager] getShopList] mutableCopy]];
    [[self tableView] reloadData];
}

#pragma IBAActions
- (IBAction)didClickedAddButton:(id)sender
{
    [self createAdditingAlertWithTitle:NSLocalizedString(@"alert.adding_new_shop.title", nil) Desciption:NSLocalizedString(@"alert.adding_new_shop.description", nil)];
}

#pragma URLValidator
- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    
    if (alertController)
    {
        UITextField *url = alertController.textFields.firstObject;
        UIAlertAction *saveAction = alertController.actions.lastObject;
        
        if ([[url.text lastPathComponent] length] > 4)
        {
            NSString *lastFourChar = [[url.text lastPathComponent] substringFromIndex:[[url.text lastPathComponent] length] - 4];
            saveAction.enabled = [lastFourChar isEqualToString:@".yml"];
        }
    }
}

- (BOOL)checkForUniquenessShop:(NSString *)shopUrl
{
    bool isOriginal;
    isOriginal = YES;
    
    for (int i = 0; i < self.shopsList.count; i++)
    {
        if ([[[[self shopsList] objectAtIndex:i] valueForKey:@"shopUrl"] isEqualToString:shopUrl])
        {
            isOriginal = NO;
        }
    }
    
    return isOriginal;
}

#pragma Gesture Recognizer Delegate
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint point = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [[self tableView] indexPathForRowAtPoint:point];
    
    if (indexPath == nil)
    {
        NSLog(@"long press on table view but not on a row");
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        [self createDeletingAlertWithTitle:[[(ShopCell *)[[self tableView] cellForRowAtIndexPath:indexPath] label] text]];
        indexPathForDeleting = indexPath;
    }
}

#pragma alert creating
- (void)createAdditingAlertWithTitle:(NSString *)title Desciption:(NSString *)description
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:description
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
    {
        [textField addTarget:self
                      action:@selector(alertTextFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        textField.keyboardType = UIKeyboardTypeAlphabet;
    }];
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.save", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action)
                                                        {
                                                            if ([self checkForUniquenessShop:alert.textFields.lastObject.text] == YES)
                                                            {
                                                                [self addShopWithUrl:alert.textFields.lastObject.text];
                                                            }
                                                            else
                                                            {
                                                                [self createDublicationShopAlert];
                                                            }
                                                        }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.cancel", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {}];
    
    [saveAction setEnabled:NO];
    [alert addAction:cancelAction];
    [alert addAction:saveAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)createDublicationShopAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"alert.error.shop_already_on_database.title", nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okayAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.okay", nil)
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction *action){}];
    [alert addAction:okayAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)createDeletingAlertWithTitle:(NSString *)shopName
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:shopName
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.delete", nil)
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action)
                                                            {
                                                                [self createSubmittingDeleteForShopAlert:shopName];
                                                            }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {}];
    
    [alert addAction:deleteAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)createSubmittingDeleteForShopAlert:(NSString *)shop
{
    NSString *title = [NSString stringWithFormat:NSLocalizedString(@"deletingShopAlert.want_to_remove_shop %@", nil), shop];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.confirm", nil)
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action)
                                                            {
                                                                [self deleteShop:shop];
                                                            }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.cancel", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {}];
    [alert addAction:cancelAction];
    [alert addAction:submitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end

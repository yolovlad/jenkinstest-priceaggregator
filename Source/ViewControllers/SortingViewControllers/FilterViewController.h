#import <UIKit/UIKit.h>
#import "FilterEnum.h"
#import "FilterCell.h"
#import "FilterSliderCell.h"
#import "RangeSliderProtocol.h"
#import "FilterManagerProtocol.h"
#import "FilterManager.h"
#import "ItemQueryManagerProtocol.h"
#import "ItemQueryManager.h"

@interface FilterViewController : UITableViewController<RangeSliderProtocol, SignatureNotificationProtocol>
@end

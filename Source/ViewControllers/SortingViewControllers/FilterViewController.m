//
//  SortingViewController.m
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "NotificationEnum.h"
#import "SignatureNotificationDelegate.h"
#import "SubscribeNotificationHelperService.h"
#import "FilterViewController.h"
@interface FilterViewController ()

@property(strong, nonatomic) SignatureNotificationService *notificationService;
@property(strong, nonatomic) NSMutableArray *shopList;

@property(weak, nonatomic) id<FilterManagerProtocol> filterManagerDelegate;
@property(strong, nonatomic) FilterManager *filterManager;

@property(weak, nonatomic) id<ItemQueryManagerProtocol> itemRequestManager;
@property(strong, nonatomic) ItemQueryManager *itemRequestManagerAnchor;

@end
@implementation FilterViewController
{
    NSArray *sortByPriceSectionItems;
    NSArray *sortByPriceSectionOptions;
    NSArray *availabilitySectionItems;
    NSArray *availabilitySectionOptions;
    
    FilterSliderCell *slider;
    
    NSMutableArray *firstSectionSelectedCellsArray;
    NSMutableArray *secondSectionSelectedCellsArray;
    NSMutableArray *thirdSectionSelectedCellsArray;

    NSInteger minimumPriceValue;
    NSInteger maximumPriceValue;
    NSInteger currentMinimumPriceValue;
    NSInteger currentMaximumPriceValue;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    UINib *filterCell = [UINib nibWithNibName:@"FilterCell" bundle:nil];
    UINib *filterSliderCell = [UINib nibWithNibName:@"FilterSliderCell" bundle:nil];
    [self.tableView registerNib:filterSliderCell forCellReuseIdentifier:@"filterSliderCell"];
    [self.tableView registerNib:filterCell forCellReuseIdentifier:@"filterCell"];
    
    [self setupOptionsArray];
    [self setFilterManager:[[FilterManager alloc] init]];
    [self setShopList:[NSMutableArray new]];
    [self setFilterManagerDelegate:[self filterManager]];
    [self resetFilterToDefaultSettings];
}

- (void)viewDidLoad
{
    [self addObservingNotificationForFilterUpdate];
    [self setItemRequestManagerAnchor:[ItemQueryManager new]];
    [self setItemRequestManager:[self itemRequestManagerAnchor]];
    [[self itemRequestManager] getInformationForFilters:^(NSArray *shopList, NSInteger minimum, NSInteger maximum)
    {
        if (maximum == 0)
        {
            maximum = 1;
        }
        
        minimumPriceValue = minimum;
        maximumPriceValue = maximum;
        currentMinimumPriceValue = minimumPriceValue;
        currentMaximumPriceValue = maximumPriceValue;
        [self setShopList:[shopList mutableCopy]];
        [self resetShopEnableList];
        [[self tableView] reloadData];
    }];
}

- (void)dealloc
{
    [[self notificationService] unsubscribeObject:self];
}

#pragma tableView dataSource/Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.section == 2)
    {
        FilterSliderCell *sliderCell = [tableView dequeueReusableCellWithIdentifier:@"filterSliderCell" forIndexPath:indexPath];
        [sliderCell configureSliderWithBordersValueMin:minimumPriceValue
                                                   Max:maximumPriceValue
                                       CurrentValueMin:currentMinimumPriceValue
                                       CurrentValueMax:currentMaximumPriceValue];
        [sliderCell setDelegate:self];
        cell = sliderCell;
        slider = sliderCell;
    }
    else
    {
        FilterCell *filterCell = [tableView dequeueReusableCellWithIdentifier:@"filterCell" forIndexPath:indexPath];
        
        switch (indexPath.section)
        {
            case 0:
                [[filterCell filterOptionLabel] setText:[sortByPriceSectionItems objectAtIndex:[indexPath row]]];
                [filterCell setOption:[sortByPriceSectionOptions objectAtIndex:[indexPath row]]];
                break;
            case 1:
                [[filterCell filterOptionLabel] setText:[availabilitySectionItems objectAtIndex:[indexPath row]]];
                [filterCell setOption:[availabilitySectionOptions objectAtIndex:[indexPath row]]];
                break;
            case 3:
                [[filterCell filterOptionLabel] setText:[[self shopList] objectAtIndex:indexPath.row]];
                break;
        }
        
        if (indexPath.section == 0)
        {
            NSNumber *rowNum = [NSNumber numberWithInteger:indexPath.row];
            
            if ([firstSectionSelectedCellsArray containsObject:rowNum])
            {
                [[self tableView] selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            else
            {
                [[self tableView] deselectRowAtIndexPath:indexPath animated:NO];
            }
        }
        
        if (indexPath.section == 1)
        {
            NSNumber *rowNsNum = [NSNumber numberWithInteger:indexPath.row];
            
            if ([secondSectionSelectedCellsArray containsObject:rowNsNum])
            {
                [[self tableView] selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            else
            {
                [[self tableView] deselectRowAtIndexPath:indexPath animated:NO];
            }
        }
        
        if (indexPath.section == 3)
        {
            NSNumber *rowNsNum = [NSNumber numberWithInteger:indexPath.row];
            
            if ([thirdSectionSelectedCellsArray containsObject:rowNsNum])
            {
                [[self tableView] selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            else
            {
                [[self tableView] deselectRowAtIndexPath:indexPath animated:NO];
            }
        }
        
        cell = filterCell;
    }
    
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        NSNumber *rowNum = [NSNumber numberWithInteger:indexPath.row];
        
        if ([firstSectionSelectedCellsArray containsObject:rowNum])
        {
            return nil;
        }
    }
    
    if (indexPath.section == 1)
    {
        NSNumber *rowNsNum = [NSNumber numberWithInteger:indexPath.row];
        
        if ([secondSectionSelectedCellsArray containsObject:rowNsNum])
        {
            return nil;
        }
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        NSNumber *rowNum = [NSNumber numberWithInteger:indexPath.row];
        [firstSectionSelectedCellsArray removeAllObjects];
        [[self filterManagerDelegate] setOption:[(FilterCell *)[tableView cellForRowAtIndexPath:indexPath] option]];
        [firstSectionSelectedCellsArray addObject:rowNum];
    }
    
    if (indexPath.section == 1)
    {
        NSNumber *rowNum = [NSNumber numberWithInteger:indexPath.row];
        [secondSectionSelectedCellsArray removeAllObjects];
        [[self filterManagerDelegate] setOption:[(FilterCell *)[tableView cellForRowAtIndexPath:indexPath] option]];
        [secondSectionSelectedCellsArray addObject:rowNum];
    }
    
    if (indexPath.section == 3)
    {
        NSNumber *rowNum = [NSNumber numberWithInteger:indexPath.row];
        [[self filterManager] setShop:[[(FilterCell *)[tableView cellForRowAtIndexPath:indexPath] filterOptionLabel] text] Enable:YES];
        [thirdSectionSelectedCellsArray addObject:rowNum];
    }
    
    [[self tableView] reloadData];
    [[self notificationService] sendNotification:filtersDidUpdate];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3)
    {
        NSNumber *rowNum = [NSNumber numberWithInteger:indexPath.row];
        [[self filterManager] setShop:[[(FilterCell *)[tableView cellForRowAtIndexPath:indexPath] filterOptionLabel] text] Enable:NO];
        [thirdSectionSelectedCellsArray removeObject:rowNum];
    }
    
    [[self tableView] reloadData];
    [[self notificationService] sendNotification:filtersDidUpdate];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    static NSInteger numberOfRows;
    
    switch (section)
    {
        case 0:
            numberOfRows =  2;
            break;
        case 1:
            numberOfRows =  2;
            break;
        case 2:
            numberOfRows =  1;
            break;
        case 3:
            numberOfRows =  [[self shopList] count];
            break;
    }
    
    return numberOfRows;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *CellIdentifier;
    
    switch (section)
    {
        case 0:
            CellIdentifier = @"sortByPrice";
            break;
        case 1:
            CellIdentifier = @"availability";
            break;
        case 2:
            CellIdentifier = @"priceRange";
            break;
        case 3:
            CellIdentifier = @"shops";
            break;
    }
    
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSInteger heightOfRows;
    
    if (indexPath.section == 2)
    {
        heightOfRows = 100.0f;
    }
    else
    {
        heightOfRows = 40.0f;
    }
    
    return heightOfRows;
}

#pragma Adding self-observing
- (void)addObservingNotificationForFilterUpdate
{
    [self setNotificationService:[SignatureNotificationService new]];
    [[self notificationService] subscribeObject:self ToNotification:madeNewSearch];
}

#pragma RangeSliderProtocol
- (void)rangeDidChangeWithMin:(CGFloat)minimum Max:(CGFloat)maximum
{
    [[self filterManagerDelegate] setRangeWithMin:(NSInteger)minimum Max:(NSInteger)maximum];
    [[self notificationService] sendNotification:filtersDidUpdate];
    [self rangeSaveCurrentValue:minimum Max:maximum];
}

- (void)rangeSaveCurrentValue:(CGFloat)minimum Max:(CGFloat)maximum
{
    currentMaximumPriceValue = maximum;
    currentMinimumPriceValue = minimum;
}

#pragma SignatureNotificationDelelegate
- (void)updateFilters
{
    [self resetFilterToDefaultSettings];
    [[self itemRequestManager] getInformationForFilters:^(NSArray *shopList, NSInteger minimum, NSInteger maximum)
    {
        if (maximum == 0)
        {
            maximum = 1;
        }
        
        minimumPriceValue = minimum;
        maximumPriceValue = maximum;
        currentMinimumPriceValue = minimumPriceValue;
        currentMaximumPriceValue = maximumPriceValue;
        [self setShopList:[shopList mutableCopy]];
        [self resetShopEnableList];
        [[self tableView] reloadData];
    }];
    [[self tableView] reloadData];
}

#pragma  self methods
- (void)resetFilterToDefaultSettings
{
    [firstSectionSelectedCellsArray removeAllObjects];
    [secondSectionSelectedCellsArray removeAllObjects];
    [thirdSectionSelectedCellsArray removeAllObjects];
    NSNumber *ascending = [NSNumber numberWithInteger:0];
    NSNumber *all = [NSNumber numberWithInteger:0];
    [firstSectionSelectedCellsArray addObject:ascending];
    [self resetShopEnableList];
    [secondSectionSelectedCellsArray addObject:all];
    [[self filterManagerDelegate] resetFilterToDefaultSettings];
}

- (void)resetShopEnableList
{
    for (NSInteger row = 0; row < self.shopList.count; row++)
    {
        NSNumber *rowNum = [NSNumber numberWithInteger:row];
        [thirdSectionSelectedCellsArray addObject:rowNum];
    }
}

- (void)setupOptionsArray
{
    minimumPriceValue = 1;
    maximumPriceValue = 2;
    currentMaximumPriceValue = maximumPriceValue;
    currentMinimumPriceValue = minimumPriceValue;
    sortByPriceSectionItems = @[NSLocalizedString(@"filter.ascending", nil), NSLocalizedString(@"filter.descending", nil)];
    sortByPriceSectionOptions = @[@"sortByPriceAscending", @"sortByPriceDescending"];
    availabilitySectionItems = @[NSLocalizedString(@"filter.all", nil), NSLocalizedString(@"filter.only_avaliable", nil)];
    availabilitySectionOptions = @[@"availabilityAll", @"availabilityOnlyAvailabile"];
    firstSectionSelectedCellsArray = [NSMutableArray new];
    secondSectionSelectedCellsArray = [NSMutableArray new];
    thirdSectionSelectedCellsArray = [NSMutableArray new];
}

@end

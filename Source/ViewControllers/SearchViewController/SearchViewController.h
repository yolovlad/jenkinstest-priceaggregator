//
//  SearchViewController.h
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import <TwitterKit/TwitterKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Accounts/AccountsDefines.h>
#import <Accounts/Accounts.h>
#import "BrowserViewController.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "NotificationEnum.h"
#import "SubscribeNotificationHelperService.h"
#import "SignatureNotificationDelegate.h"
#import "OfferCell.h"
#import "OfferActionProtocol.h"
#import "ShopQueryManager.h"
#import "ShopQueryManagerProtocol.h"
#import "SWRevealViewController.h"
#import <UIKit/UIKit.h>
#import "ItemQueryManagerProtocol.h"
#import "ItemQueryManager.h"
#import <STTwitter.h>
@interface SearchViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, SignatureNotificationProtocol, OfferActionProtocol, UICollectionViewDelegateFlowLayout>

@end

//
//  SearchViewController.m
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import <Social/Social.h>
#import "SearchViewController.h"
@interface SearchViewController ()<UISearchBarDelegate, UIDocumentInteractionControllerDelegate>

@property(weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(weak, nonatomic) IBOutlet UIBarButtonItem *side;
@property(weak, nonatomic) IBOutlet UIBarButtonItem *settingButton;
@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic, retain) UIDocumentInteractionController *documentController;

@property(strong, nonatomic) NSMutableArray *itemList;

@property(strong, nonatomic) SignatureNotificationService *notificationService;

@property(weak, nonatomic) id<ItemQueryManagerProtocol> itemRequestManager;
@property(strong, nonatomic) ItemQueryManager *itemRequestManagerAnchor;

@property(strong, nonatomic) ShopQueryManager *shopsRequestManagerAnchor;
@property(weak, nonatomic) id<ShopQueryManagerProtocol> shopsManager;

@property(nonatomic, strong) STTwitterAPI *twitter;

@end
@implementation SearchViewController
{
    UIViewController *sortingViewController;
    NSURL *urlForOffer;
    ACAccountStore  *myStore;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    sortingViewController = [storyboard instantiateViewControllerWithIdentifier:@"SortingVC"];
}

- (void)dealloc
{
    [[self notificationService] unsubscribeObject:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setItemRequestManagerAnchor:[ItemQueryManager new]];
    [self setItemRequestManager:[self itemRequestManagerAnchor]];
    [self setShopsRequestManagerAnchor:[ShopQueryManager new]];
    [self setShopsManager:[self shopsRequestManagerAnchor]];
    [self initSWRevealViewController];
    
    UINib *cellNib = [UINib nibWithNibName:@"OfferCell" bundle:nil];
    [[self collectionView] registerNib:cellNib forCellWithReuseIdentifier:@"OfferCell"];
    [self addObservingNotificationForFilterUpdate];
    self.collectionView.emptyDataSetSource = self;
    self.collectionView.emptyDataSetDelegate = self;
    [[self itemRequestManager] getItemListForRequest:@"":^(NSArray *respond)
    {
        [self updateItemList:respond];
    }];
}

#pragma Collectionview Delegate/DataSource methods
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OfferCell *cell = [[self collectionView] dequeueReusableCellWithReuseIdentifier:@"OfferCell" forIndexPath:indexPath];
    [cell setupCellWithOffer:[[self itemList] objectAtIndex:[indexPath row]]];
    [cell setDelegate:self];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self itemList] count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma searchBar Delegate Methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [[self itemRequestManager] getItemListForRequest:[searchBar text]:^(NSArray *respond)
    {
        [self updateItemList:respond];
    }];
    [[self notificationService] sendNotification:madeNewSearch];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [[self itemRequestManager] getItemListForRequest:searchText:^(NSArray *respond)
    {
        [self updateItemList:respond];
    }];
    [[self notificationService] sendNotification:madeNewSearch];
}

#pragma SWRevealViewController init method
- (void)initSWRevealViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController)
    {
        [[self side] setTarget:self.revealViewController];
        [[self side] setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
        [revealViewController setRightViewController:sortingViewController];
        [[self settingButton] setTarget:self.revealViewController];
        [[self settingButton] setAction:@selector(rightRevealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma Adding self-observing
- (void)addObservingNotificationForFilterUpdate
{
    [self setNotificationService:[SignatureNotificationService new]];
    [[self notificationService] subscribeObject:self ToNotification:filtersDidUpdate];
}

#pragma SignatureNotificationDelelegate
- (void)makeNewRequest
{
    [[self itemRequestManager] getItemListForRequest:self.searchBar.text:^(NSArray *respond)
    {
        [self updateItemList:respond];
    }];
}

#pragma OfferActionProtocol
- (void)addCellToFavorite:(OfferCell *)cell
{
    [[self shopsManager] addFavoriteItem:[cell identifier] ToShop:[cell shopName]];
    [[self itemRequestManager] getItemListForRequest:self.searchBar.text:^(NSArray *respond)
    {
        [self updateItemList:respond];
    }];
}

- (void)openURL:(NSURL *)url
{
    urlForOffer = url;
    [self performSegueWithIdentifier:@"segueToBrowser" sender:self];
}

- (void)deleteCellFromFavorite:(OfferCell *)cell
{
    [self createDeletingAlertForFavoriteCell:cell];
}

- (void)shareCell:(OfferCell *)cell
{
    [self creatingSharingAlertWithCellData:cell];
}

#pragma DNZEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"error.no_matches_found", nil);
    NSDictionary *attributes = @{NSFontAttributeName : [UIFont boldSystemFontOfSize:18.0f], NSForegroundColorAttributeName : [UIColor darkGrayColor]};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
{
    return YES;
}

#pragma creatingAlertViews
- (void)createDeletingAlertForFavoriteCell:(OfferCell *)cell
{
    NSString *cancel = NSLocalizedString(@"alert.cancel", nil);
    NSString *comfirm = NSLocalizedString(@"alert.confirm", nil);
    NSString *title = NSLocalizedString(@"alert.deleting_from_favorite.title", nil);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:comfirm
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action)
                                                            {
                                                                [[self shopsManager] deleteFavoriteItem:[cell identifier] FromShop:[cell shopName]];
                                                                [[self itemRequestManager] getItemListForRequest:self.searchBar.text:^(NSArray *respond)
                                                                {
                                                                    [self updateItemList:respond];
                                                                }];
                                                            }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancel
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {}];
    [alert addAction:cancelAction];
    [alert addAction:submitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)creatingSharingAlertWithCellData:(OfferCell *)cell
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    NSString *cancel = NSLocalizedString(@"alert.cancel", nil);
    NSString *shareTitle = NSLocalizedString(@"alert.share.title", nil);

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:shareTitle
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancel
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action) {}];
    [alert addAction:[self createFacebookActionWithCellData:cell]];
    [alert addAction:[self createTwitterActiontWithCellData:cell]];
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        [alert addAction:[self createInstagramActiontWithCellData:cell]];

    }

    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (UIAlertAction *)createFacebookActionWithCellData:(OfferCell *)cell
{
    NSString *facebook = NSLocalizedString(@"alert.action.facebook", nil);
    UIAlertAction *facebookAction = [UIAlertAction actionWithTitle:facebook
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action)
                                                            {
                                                                FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                                                content.contentURL = [NSURL URLWithString:[cell url]];
                                                                [FBSDKShareDialog showFromViewController:self
                                                                                             withContent:content
                                                                                                delegate:nil];
                                                            }];
    return facebookAction;
}

- (UIAlertAction *)createInstagramActiontWithCellData:(OfferCell *)cell
{
    NSString *instagram = NSLocalizedString(@"alert.action.instagram", nil);
    UIAlertAction *instagramAction = [UIAlertAction actionWithTitle:instagram
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action)
                                                            {
                                                                NSURL *photoURL = [cell photoURL];
                                                                NSData *imageData = [[NSData alloc] initWithContentsOfURL:photoURL];
                                                                UIImage *imgShare = [[UIImage alloc] initWithData:imageData];
                                                                UIImage *imageToUse = imgShare;
                                                                NSString *documentDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
                                                                NSString *saveImagePath = [documentDirectory stringByAppendingPathComponent:@"Image.igo"];
                                                                imageData = UIImagePNGRepresentation(imageToUse);
                                                                [imageData writeToFile:saveImagePath atomically:YES];
                                                                NSURL *imageURL = [NSURL fileURLWithPath:saveImagePath];
                                                                self.documentController = [[UIDocumentInteractionController alloc] init];
                                                                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
                                                                self.documentController.delegate = self;
                                                                self.documentController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"Testing"], @"InstagramCaption", nil];
                                                                self.documentController.UTI = @"com.instagram.exclusivegram";
                                                                UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
                                                                [self.documentController presentOpenInMenuFromRect:CGRectMake(1, 1, 1, 1) inView:vc.view animated:YES];
                                                            }];
    return instagramAction;
}

- (UIAlertAction *)createTwitterActiontWithCellData:(OfferCell *)cell
{
    NSString *twitter = NSLocalizedString(@"alert.action.twitter", nil);
    UIAlertAction *twitterAction = [UIAlertAction actionWithTitle:twitter
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action)
                                                            {
                                                                [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error)
                                                                {
                                                                    if (session)
                                                                    {
                                                                        TWTRComposer *composer = [[TWTRComposer alloc] init];
                                                                        [composer setText:[cell url]];
                                                                        [composer setImage:[[cell image] image]];
                                                                        [composer showFromViewController:self completion:^(TWTRComposerResult result)
                                                                        {}];
                                                                    }
                                                                }];
                                                            }];
    return twitterAction;
}

- (void)createInstagramErrorAlert
{
    NSString *comfirm = NSLocalizedString(@"alert.okay", nil);
    NSString *errorString = NSLocalizedString(@"alert.error.instagram", nil);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:errorString
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *comfirmAction = [UIAlertAction actionWithTitle:comfirm
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {}];
    [alert addAction:comfirmAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma prapare for segue methods
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueToBrowser"])
    {
        BrowserViewController *browserViewController = [segue destinationViewController];
        [browserViewController setCurrentURL:urlForOffer];
    }
}

#pragma mark - CollectionFlowLayoutDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(self.collectionView.bounds.size.width - 40, 240);
    return size;
}

#pragma self methods
- (void)updateItemList:(NSArray *)newItemList
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        [self setItemList:[newItemList mutableCopy]];
        [[self collectionView] reloadData];
    });
}
@end

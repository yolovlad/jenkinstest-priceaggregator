//
//  YMLParser.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 06/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "Offer.h"
#import "ParserProtocol.h"
#import <Foundation/Foundation.h>

@interface YMLParser : NSObject<NSXMLParserDelegate, ParserProtocol>

@end

//
//  YMLParser.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 06/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "YMLParser.h"
@interface YMLParser ()

@property(strong, nonatomic)  NSMutableArray<Offer *> *offersArray;
@property(strong, nonatomic)  NSMutableArray *categoriesArray;

@end
@implementation YMLParser
{
    Offer *offer;
    NSString *currentElement;
    NSString *shopName;
    NSMutableDictionary *category;
    bool didParseShopName;
}

- (void)deletingCache
{
    category = [NSMutableDictionary new];
    [[self offersArray] removeAllObjects];
    [[self categoriesArray] removeAllObjects];
    shopName = nil;
    didParseShopName = NO;
}

#pragma NSXMLParser Delegate
- (void)parseFile:(NSString *)ymlPath :(void (^)(NSString *, NSError *, NSArray *))parsingFinished
{
    self.offersArray  = [[NSMutableArray alloc] init];
    self.categoriesArray  = [[NSMutableArray alloc] init];
    [self deletingCache];

    NSURL *xmlURL = [NSURL URLWithString:ymlPath];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:xmlURL];
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    parser.delegate = self;
    [parser parse];
    NSError *error = [parser parserError];
    
    if (error)
    {
        NSLog(@"Error %@", error);
    }
    
    parsingFinished(shopName, error, [self uniteItems:self.offersArray WithCategories:self.categoriesArray]);
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    currentElement = elementName;
    
    if ([elementName isEqualToString:@"offer"])
    {
        offer = [[Offer alloc] init];
        offer.available = [[attributeDict objectForKey:@"available"] boolValue];
        offer.offerId = [attributeDict objectForKey:@"id"];
        offer.shopName = shopName;
    }
    else if ([elementName isEqualToString:@"category"])
    {
        category = [NSMutableDictionary new];
        [category setObject:[attributeDict objectForKey:@"id"] forKey:@"id"];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length < 1)
        return;
    
    if ([currentElement isEqualToString:@"url"])
    {
        offer.url = string;
    }
    else if ([currentElement isEqualToString:@"price"])
    {
        offer.price = [string intValue];
    }
    else if ([currentElement isEqualToString:@"categoryId"])
    {
        offer.category = string;
    }
    else if ([currentElement isEqualToString:@"picture"])
    {
        offer.imageUrl = string;
    }
    else if ([currentElement isEqualToString:@"name"])
    {
        if (didParseShopName)
        {
            offer.name = string;
        }
        else
        {
            shopName = string;
            didParseShopName = YES;
        }
    }
    else if ([currentElement isEqualToString:@"category"])
    {
        if (string != nil)
        {
            [category setObject:string forKey:@"name"];
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"offer"])
    {
        [[self offersArray] addObject:offer];
    }
    
    if ([elementName isEqualToString:@"category"])
    {
        [[self categoriesArray] addObject:category];
    }
}

#pragma self methods
- (NSArray *)uniteItems:(NSMutableArray *)itemsArray WithCategories:(NSMutableArray *)categories
{
    for (int i = 0; i < itemsArray.count; i++)
    {
        for (int j = 0; j < categories.count; j++)
        {
            if ([[[itemsArray objectAtIndex:i] category] isEqualToString:[[[self categoriesArray] objectAtIndex:j] valueForKey:@"id"]])
            {
                [[itemsArray objectAtIndex:i] setCategory:[[categories objectAtIndex:j] valueForKey:@"name"]];
            }
        }
    }
    
    return itemsArray;
}

@end

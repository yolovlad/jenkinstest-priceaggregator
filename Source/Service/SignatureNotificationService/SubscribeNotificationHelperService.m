//
//  SignatureNotificationService.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 13/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "SubscribeNotificationHelperService.h"

@implementation SignatureNotificationService
- (instancetype)init
{
    return self;
}

- (void)subscribeObject:(id)object ToNotification:(Notifications)notification
{
    NSString *notificationString;
    
    switch (notification)
    {
        case filtersDidUpdate:
            notificationString = @"filtersDidUpdate";
            [[NSNotificationCenter defaultCenter] addObserver:object
                                                     selector:@selector(makeNewRequest)
                                                         name:notificationString
                                                       object:nil];
            break;
        case madeNewSearch:
            notificationString = @"madeNewSearch";
            [[NSNotificationCenter defaultCenter] addObserver:object
                                                 selector:@selector(updateFilters)
                                                     name:notificationString
                                                   object:nil];
            break;
    }
}

- (void)unsubscribeObject:(id)object
{
    [[NSNotificationCenter defaultCenter] removeObserver:object];
}

- (void)sendNotification:(Notifications)notification
{
    NSString *notificationString;
    
    switch (notification)
    {
        case filtersDidUpdate:
            notificationString = @"filtersDidUpdate";
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationString object:nil];
            break;
        case madeNewSearch:
            notificationString = @"madeNewSearch";
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationString object:nil];
            break;
    }
}

@end

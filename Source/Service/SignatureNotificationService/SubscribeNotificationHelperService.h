//
//  SignatureNotificationService.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 13/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "NotificationEnum.h"
#import "SignatureNotificationDelegate.h"
#import <Foundation/Foundation.h>

@interface SignatureNotificationService : NSObject

- (void)subscribeObject:(id)object ToNotification:(Notifications)notification;
- (void)unsubscribeObject:(id)object;
- (void)sendNotification:(Notifications)notification;

@end

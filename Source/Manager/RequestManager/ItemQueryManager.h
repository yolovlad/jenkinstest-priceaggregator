//
//  RequestManager.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 10/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Filter.h"
#import "FilterManagerProtocol.h"
#import "FilterManager.h"
#import "ItemQueryManagerProtocol.h"
#import "ItemsControllerProtocol.h"
#import "StorageController.h"

@interface ItemQueryManager : NSObject<ItemQueryManagerProtocol>

@end

#import "ItemQueryManager.h"
@interface ItemQueryManager ()

@property(weak, nonatomic) id<ItemsControllerProtocol> storageController;
@property(strong, nonatomic) StorageController *storageControllerAnchor;

@property(weak, nonatomic) id<FilterManagerProtocol> filterManager;
@property(strong, nonatomic) FilterManager *filterManagerAnchor;

@end
@implementation ItemQueryManager

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self setStorageControllerAnchor:[StorageController new]];
        [self setStorageController:[self storageControllerAnchor]];
        [self setFilterManagerAnchor:[FilterManager new]];
        [self setFilterManager:[self filterManagerAnchor]];
    }
    
    return self;
}

#pragma ItemRequesrManagerProtocol
- (void)getFavoriteItemList:(void (^)(NSArray *))block
{
    [[self storageController] getFavoriteItemList:^(NSArray *results)
    {
        block(results);
    }];
}

- (void)getItemListForRequest:(NSString *)query :(void (^)(NSArray *))block
{
    if (query == nil)
    {
        query = @"";
    }
    
    [[self storageController] getItemListForRequest:query WithFilters:[[self filterManager] getFilters]:^(NSArray *arrayOfResult)
    {
        block(arrayOfResult);
    }];
}

- (void)getInformationForFilters:(void (^)(NSArray *shopList, NSInteger minimum, NSInteger maximum))block
{
    [[self filterManager] resetFilterToDefaultSettings];
    [[self storageController] getItemListForRequest:@"" WithFilters:[[self filterManager] getFilters]:^(NSArray *arrayOfResult)
    {
        NSUInteger maximumValue = 0;
        NSUInteger minimumValue = 0;
        NSMutableArray *shopList = [NSMutableArray new];
        NSMutableArray *result = [arrayOfResult mutableCopy];
        NSMutableArray *sortedArray = [NSMutableArray new];
        
        for (int i = 0; i < result.count; i++)
        {
            if ([[result objectAtIndex:i] price] > maximumValue)
            {
                maximumValue = [[result objectAtIndex:i] price];
            }
            
            if ([[result objectAtIndex:i] price] < minimumValue)
            {
                maximumValue = [[result objectAtIndex:i] price];
            }
            
            if (![shopList containsObject:[[result objectAtIndex:i] shopName]])
            {
                [shopList addObject:[[result objectAtIndex:i] shopName]];
            }
        }
        
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"price"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
        sortedArray = [[result sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        minimumValue = [[sortedArray firstObject] price];
        maximumValue = [[sortedArray lastObject] price];
        [[self filterManager] setRangeWithMin:minimumValue Max:maximumValue];
        block(shopList, minimumValue, maximumValue);
    }];
}

@end

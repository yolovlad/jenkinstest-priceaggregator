#define SHOPLIST @"shopList"

#import "ShopRepository.h"
@interface ShopRepository ()

@property(weak, nonatomic) id<ItemsControllerProtocol> itemsStorageController;
@property(weak, nonatomic) NSUserDefaults *defaults;

@end
@implementation ShopRepository

- (instancetype)initWithStorageController:(StorageController *)storageController;
{
    self = [super init];
    [self setDefaults:[NSUserDefaults standardUserDefaults]];
    [self setItemsStorageController:storageController];
    
    if (![[self defaults] arrayForKey:SHOPLIST])
    {
        NSMutableArray *array = [NSMutableArray new];
        [[NSUserDefaults standardUserDefaults] setObject:array forKey:SHOPLIST];
    }
    
    return self;
}

#pragma ShopsRepositoryProtocol
- (void)addShopToStorage:(NSString *) shopURL:(void (^)(NSArray *))block
{
    NSArray *shopList;
    NSMutableArray *shopListMutable;
    shopList = [[self defaults] objectForKey:SHOPLIST];
    shopListMutable = [shopList mutableCopy];
    
    NSMutableDictionary *newShop = [NSMutableDictionary new];
    
    [newShop setValue:[NSString stringWithFormat:@"%@", shopURL] forKey:@"shopUrl"];
    [newShop setValue:shopURL forKey:@"shopName"];
    [newShop setObject:[NSNumber numberWithBool:NO] forKey:@"download"];
    [newShop setObject:[NSNumber numberWithBool:NO] forKey:@"validate"];
    
    NSMutableArray *favorites = [NSMutableArray new];
    [newShop setObject:favorites forKey:@"favorites"];
    
    [shopListMutable addObject:newShop];
    [[self defaults] setObject:shopListMutable forKey:SHOPLIST];
    [[self defaults] synchronize];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        [[self itemsStorageController] addShopAndItemsToStorage:shopURL:^(NSString *shopName, NSError *error)
        {
            for (int i = 0; i < shopListMutable.count; i++)
            {
                if ([[[shopListMutable objectAtIndex:i] valueForKey:@"shopUrl"] isEqualToString:shopURL])
                {
                    if (!error)
                    {
                        [[shopListMutable objectAtIndex:i] setValue:shopName forKey:@"shopName"];
                        [[shopListMutable objectAtIndex:i] setValue:[NSNumber numberWithBool:YES] forKey:@"download"];
                        [[shopListMutable objectAtIndex:i] setValue:[NSNumber numberWithBool:YES] forKey:@"validate"];
                        [[self defaults] setObject:shopListMutable forKey:SHOPLIST];
                        [[self defaults] synchronize];
                    }
                    else
                    {
                        [[shopListMutable objectAtIndex:i] setValue:[NSNumber numberWithBool:YES] forKey:@"download"];
                        [[shopListMutable objectAtIndex:i] setValue:[NSNumber numberWithBool:NO] forKey:@"validate"];
                        [[self defaults] setObject:shopListMutable forKey:SHOPLIST];
                        [[self defaults] synchronize];
                    }
                    
                    block([[self defaults] objectForKey:SHOPLIST]);
                }
            }
        }];
    });
}

- (void)deleteShopFromStorage:(NSString *)shopName
{
    NSArray *shopList;
    NSMutableArray *shopListMutable;
    shopList = [[self defaults] objectForKey:SHOPLIST];
    shopListMutable = [shopList mutableCopy];
    
    for (int i = 0; i < shopListMutable.count; i++)
    {
        if ([[[shopListMutable objectAtIndex:i] valueForKey:@"shopName"] isEqualToString:shopName])
        {
            [shopListMutable removeObjectAtIndex:i];
        }
    }

    [[self defaults] setObject:shopListMutable forKey:SHOPLIST];
    [[self defaults] synchronize];
}

- (NSArray *)getShopList
{
    NSArray *dictValues;
    dictValues = [[self defaults] objectForKey:SHOPLIST];
    return dictValues;
}

- (void)addFavoriteItem:(NSString *)offerId ToShop:(NSString *)shopName
{
    NSArray *shopList;
    NSMutableArray *shopListMutable;
    NSMutableArray *favoritesArray;
    NSMutableDictionary *dictionary;

    shopList = [[self defaults] objectForKey:SHOPLIST];
    shopListMutable = [shopList mutableCopy];
    
    for (int i = 0; i < shopListMutable.count; i++)
    {
        if ([[[shopListMutable objectAtIndex:i] valueForKey:@"shopName"] isEqualToString:shopName])
        {
            favoritesArray =  [[[shopListMutable objectAtIndex:i] objectForKey:@"favorites"] mutableCopy];
            
            if (![favoritesArray containsObject:offerId])
            {
                [favoritesArray addObject:offerId];
            }
            
            dictionary =  [[shopListMutable objectAtIndex:i] mutableCopy];
            [dictionary setObject:favoritesArray forKey:@"favorites"];
            [shopListMutable removeObjectAtIndex:i];
            [shopListMutable insertObject:dictionary atIndex:i];
            [[self defaults] setObject:shopListMutable forKey:SHOPLIST];
            [[self defaults] synchronize];
        }
    }
}

- (void)deleteFavoriteItem:(NSString *)offerId FromShop:(NSString *)shopName
{
    NSArray *shopList;
    NSMutableArray *shopListMutable;
    NSMutableArray *favoritesArray;
    NSMutableDictionary *dictionary;
    
    shopList = [[self defaults] objectForKey:SHOPLIST];
    shopListMutable = [shopList mutableCopy];
    
    for (int i = 0; i < shopListMutable.count; i++)
    {
        if ([[[shopListMutable objectAtIndex:i] valueForKey:@"shopName"] isEqualToString:shopName])
        {
            favoritesArray = [[[shopListMutable objectAtIndex:i] objectForKey:@"favorites"] mutableCopy];
            
            if ([favoritesArray containsObject:offerId])
            {
                [favoritesArray removeObject:offerId];
            }
            
            dictionary =  [[shopListMutable objectAtIndex:i] mutableCopy];
            [dictionary setObject:favoritesArray forKey:@"favorites"];
            [shopListMutable removeObjectAtIndex:i];
            [shopListMutable insertObject:dictionary atIndex:i];
            [[self defaults] setObject:shopListMutable forKey:SHOPLIST];
            [[self defaults] synchronize];
        }
    }
}

@end

//
//  StoreManager.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 05/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "StorageController.h"
#import "ShopsRepositoryProtocol.h"
#import <Foundation/Foundation.h>
#import "ItemsRepository.h"
@class StorageController;
@interface ShopRepository : NSObject<ShopsRepositoryProtocol>

- (instancetype)initWithStorageController:(StorageController *)storageController;

@end

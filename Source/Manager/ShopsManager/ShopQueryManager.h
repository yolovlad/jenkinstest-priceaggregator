//
//  ShopsManager.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 10/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "ShopQueryManagerProtocol.h"
#import "ShopsRepositoryProtocol.h"
#import <Foundation/Foundation.h>

@interface ShopQueryManager : NSObject<ShopQueryManagerProtocol>

@end

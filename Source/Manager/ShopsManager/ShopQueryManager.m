//
//  ShopsManager.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 10/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "ShopRepository.h"
#import "ShopQueryManager.h"
#import "StorageController.h"
@interface ShopQueryManager ()

@property(weak, nonatomic) id<ShopsControllerProtocol> shopController;
@property(strong, nonatomic) StorageController *storageControllerAnchor;

@end
@implementation ShopQueryManager

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self setStorageControllerAnchor:[StorageController new]];
        [self setShopController:(id<ShopsControllerProtocol>)[self storageControllerAnchor]];
    }
    
    return self;
}

#pragma ShopRequestManagerProtocol
- (void)addShop:(NSString *)shopURL :(void (^)(NSArray *))block
{
    [[self shopController] addShop:shopURL:^(NSArray *storeList)
    {
        block(storeList);
    }];
}

- (void)deleteShop:(NSString *)shopName
{
    [[self shopController] deleteShop:shopName];
}

- (NSArray *)getShopList
{
    return [[self shopController] getShopList];
}

- (void)addFavoriteItem:(NSString *)offerId ToShop:(NSString *)shopName
{
    [[self shopController] addFavoriteItem:offerId ToShop:shopName];
}

- (void)deleteFavoriteItem:(NSString *)offerId FromShop:(NSString *)shopName
{
    [[self shopController] deleteFavoriteItem:offerId FromShop:shopName];
}

@end

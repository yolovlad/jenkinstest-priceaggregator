//
//  FilterManager.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 05/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "FilterManager.h"
@interface FilterManager ()

@end
@implementation FilterManager

#pragma FilterManagerProtocol
- (Filter *)getFilters
{
    return [Filter sharedFilter];
}

- (void)setOption:(NSString *)option
{
    if ([option isEqualToString:@"sortByPriceAscending"])
    {
        [[Filter sharedFilter] setSortByPrice:sortByPriceAscending];
    }
    else if ([option isEqualToString:@"sortByPriceDescending"])
    {
        [[Filter sharedFilter] setSortByPrice:sortByPriceDescending];
    }
    else if ([option isEqualToString:@"availabilityAll"])
    {
        [[Filter sharedFilter] setAvailability:availabilityAll];
    }
    else if ([option isEqualToString:@"availabilityOnlyAvailabile"])
    {
        [[Filter sharedFilter] setAvailability:availabilityOnlyAvailabile];
    }
}

- (void)setRangeWithMin:(NSInteger)minimum Max:(NSInteger)maximum
{
    [[Filter sharedFilter] setMinimumRange:minimum];
    [[Filter sharedFilter] setMaximumRange:maximum];
}

- (void)resetFilterToDefaultSettings;
{
    [[Filter sharedFilter] setSortByPrice:sortByPriceAscending];
    [[Filter sharedFilter] setAvailability:availabilityAll];
    [[Filter sharedFilter] setMinimumRange:NSIntegerMin];
    [[Filter sharedFilter] setMaximumRange:NSIntegerMax];
    [[[Filter sharedFilter] shops] removeAllObjects];
}

- (void)setShop:(NSString *)shopName Enable:(BOOL)enable
{
    if (enable == YES)
    {
        if ([[[Filter sharedFilter] shops] containsObject:shopName])
        {
            [[[Filter sharedFilter] shops] removeObject:shopName];
        }
    }
    else
    {
        [[[Filter sharedFilter] shops] addObject:shopName];
    }
}

@end

//
//  FilterManager.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 05/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "FilterEnum.h"
#import "Filter.h"
#import <Foundation/Foundation.h>
#import "FilterManagerProtocol.h"

@interface FilterManager : NSObject<FilterManagerProtocol>

@end

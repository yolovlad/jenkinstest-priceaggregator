//
//  Repository.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 06/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "Storage.h"
#import "Offer.h"
#import "ParserProtocol.h"
#import "ShopsControllerProtocol.h"
#import "ItemsRepository.h"
#import "ShopRepository.h"

@interface ItemsRepository ()

@property(weak, nonatomic) id<ShopsControllerProtocol> shopController;
@property(weak, nonatomic) id<ParserProtocol> parser;

@property(strong, nonatomic) Storage *storage;
@property(strong, nonatomic) YMLParser *parserAnchor;
@property(strong, nonatomic) NSMutableArray *array;

@end
@implementation ItemsRepository

- (instancetype)initWithStorageController:(StorageController *)storageController
{
    self = [super init];
    
    if (self)
    {
        [self setShopController:storageController];
        [self setParserAnchor:[YMLParser new]];
        [self setParser:[self parserAnchor]];
        [self setStorage:[Storage sharedStorage]];
    }
    
    return self;
}

#pragma ItemsRepositoryProtocol
- (void)addShopAndItemsToStorage:(NSString *)shopURL :(void (^)(NSString *, NSError *))didAddingShop
{
    self.array = [[NSMutableArray alloc] init];
    [[self parser] parseFile:(NSString *)shopURL:^(NSString *shopName, NSError *error, NSArray *itemList)
    {
        if (shopName != nil)
        {
            [[[self storage] storageDictionary] setObject:itemList forKey:shopName];
        }
        
        didAddingShop(shopName, error);
    }];
}

- (void)removeItemsFromStorageForShop:(NSString *)shopURL
{
    if ([[[self storage] storageDictionary] objectForKey:shopURL] != nil)
    {
        [[[self storage] storageDictionary] removeObjectForKey:shopURL];
    }
}

- (void)getItemsFromStorageForRequest:(NSString *)request WithFilter:(Filter *)filter :(void (^)(NSArray *))block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        [self checkStorageForDataStock];
    
        NSMutableArray *temporaryArray = [[NSMutableArray alloc] init];
        NSMutableArray<Offer *> *arrayOfResults = [[NSMutableArray alloc] init];
        NSMutableArray *values = [[NSMutableArray alloc] init];
        
        values = [[[[self storage] storageDictionary] allValues] mutableCopy];
        
        for (int i = 0; i < values.count; i++)
        {
            for (int j = 0; j < [[values objectAtIndex:i] count]; j++)
            {
                [temporaryArray addObject:[[values objectAtIndex:i] objectAtIndex:j]];
            }
        }
        
        NSMutableArray *arrayWithoutBlackList = [NSMutableArray new];
        
        for (int i = 0; i < temporaryArray.count; i++)
        {
            if (filter.shops.count == 0)
            {
                [arrayWithoutBlackList addObject:[temporaryArray objectAtIndex:i]];
            }
            else if (![[filter shops] containsObject:[[temporaryArray objectAtIndex:i] shopName]])
            {
                [arrayWithoutBlackList addObject:[temporaryArray objectAtIndex:i]];
            }
        }
        
        if ([filter availability] == availabilityOnlyAvailabile)
        {
            for (int i = 0; i < arrayWithoutBlackList.count; i++)
            {
                if ([[arrayWithoutBlackList objectAtIndex:i] available] == YES)
                {
                    [arrayOfResults addObject:[arrayWithoutBlackList objectAtIndex:i]];
                }
            }
        }
        else
        {
            for (int i = 0; i < arrayWithoutBlackList.count; i++)
            {
                [arrayOfResults addObject:[arrayWithoutBlackList objectAtIndex:i]];
            }
        }
        
        NSMutableArray *arrayWithPredicate = [NSMutableArray new];
        
        for (int i = 0; i < arrayOfResults.count; i++)
        {
            if (request != nil)
            {
                if ([request isEqualToString:@""])
                {
                    [arrayWithPredicate addObject:[arrayOfResults objectAtIndex:i]];
                }
                else if (([[[arrayOfResults objectAtIndex:i] name] rangeOfString:request].location != NSNotFound))
                {
                    [arrayWithPredicate addObject:[arrayOfResults objectAtIndex:i]];
                }
            }
        }
        
        NSMutableArray *finalRusultArrayWithPredicate = [NSMutableArray new];
        
        if ([filter sortByPrice] == sortByPriceAscending)
        {
            NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"price"  ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
            finalRusultArrayWithPredicate  = [[arrayWithPredicate sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        }
        else
        {
            NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"price" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
            finalRusultArrayWithPredicate  = [[arrayWithPredicate sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        }
        
        NSMutableArray *finalRusulteWithPriceFilter = [NSMutableArray new];
        
        for (int i = 0; i < finalRusultArrayWithPredicate.count; i++)
        {
            if (filter.maximumRange != 0)
            {
                if (((NSInteger)[[finalRusultArrayWithPredicate objectAtIndex:i] price] >= (NSInteger)filter.minimumRange)
                    && ((NSInteger)[[finalRusultArrayWithPredicate objectAtIndex:i] price] <= (NSInteger)filter.maximumRange))
                {
                    [finalRusulteWithPriceFilter addObject:[finalRusultArrayWithPredicate objectAtIndex:i]];
                }
            }
        }
        
        finalRusulteWithPriceFilter = [[self updateFavoriteItems:finalRusulteWithPriceFilter] mutableCopy];
        block(finalRusulteWithPriceFilter);
    });
}

- (void)getAllItems:(void (^)(NSArray *))block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        [self checkStorageForDataStock];
        NSMutableArray *temporaryArray = [[NSMutableArray alloc] init];
        NSMutableArray *values = [[NSMutableArray alloc] init];
        values = [[[[self storage] storageDictionary] allValues] mutableCopy];
        
        for (int i = 0; i < values.count; i++)
        {
            for (int j = 0; j < [[values objectAtIndex:i] count]; j++)
            {
                [temporaryArray addObject:[[values objectAtIndex:i] objectAtIndex:j]];
            }
        }
        
        temporaryArray = [[self updateFavoriteItems:temporaryArray] mutableCopy];
        block(temporaryArray);
    });
}

#pragma self methods
- (void)checkStorageForDataStock
{
    NSArray *shopDataArray = [[self shopController] getShopList];
    
    for (int i = 0; i < shopDataArray.count; i++)
    {
        if (![[[[self storage] storageDictionary] allKeys] containsObject:[[shopDataArray objectAtIndex:i] objectForKey:@"shopName"]])
        {
            [self addShopAndItemsToStorage:[[shopDataArray objectAtIndex:i] objectForKey:@"shopUrl"]:^(NSString *shopName, NSError *error)
            {
                NSLog(@"скачал %@", shopName);
            }];
        }
    }
}

- (NSArray *)updateFavoriteItems:(NSArray *)arrayToCompare
{
    NSMutableArray *compareArray = [arrayToCompare mutableCopy];
    NSMutableArray *shopArray = [[[self shopController] getShopList] mutableCopy];
    
    for (int shop = 0; shop < shopArray.count; shop++)
    {
        NSMutableArray *arrayOfResult = [[shopArray objectAtIndex:shop] objectForKey:@"favorites"];
        
        if (arrayOfResult.count != 0)
        {
            for (int i = 0; i < compareArray.count; i++)
            {
                if ([[[compareArray objectAtIndex:i] shopName] isEqual:[[shopArray objectAtIndex:shop] objectForKey:@"shopName"]])
                {
                    if ([arrayOfResult containsObject:[[compareArray objectAtIndex:i] offerId]])
                    {
                        [[compareArray objectAtIndex:i] setFavorite:YES];
                    }
                    else
                    {
                        [[compareArray objectAtIndex:i] setFavorite:NO];
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < compareArray.count; i++)
            {
                if ([[[compareArray objectAtIndex:i] shopName] isEqual:[[shopArray objectAtIndex:shop] objectForKey:@"shopName"]])
                {
                    [[compareArray objectAtIndex:i] setFavorite:NO];
                }
            }
        }
    }
    
    return compareArray;
}

@end

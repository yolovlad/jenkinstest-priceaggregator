#import "ShopsControllerProtocol.h"
#import "ItemsRepositoryProtocol.h"

@interface ItemsRepository : NSObject<ItemsRepositoryProtocol>

- (instancetype)initWithStorageController:(id<ShopsControllerProtocol>)shopsStorageController;

@end

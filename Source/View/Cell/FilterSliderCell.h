//
//  filterSliderCell.h
//  price-aggregator
//
//  Created by Yalovenko on 30.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "RangeSliderProtocol.h"
#import "NMRangeSlider.h"
#import <UIKit/UIKit.h>

@interface FilterSliderCell : UITableViewCell

@property(weak, nonatomic) id<RangeSliderProtocol> delegate;

- (void)configureSliderWithBordersValueMin:(CGFloat)minimum Max:(CGFloat)maximum CurrentValueMin:(CGFloat)currentMinimum CurrentValueMax:(CGFloat)currentMaximum;
@end

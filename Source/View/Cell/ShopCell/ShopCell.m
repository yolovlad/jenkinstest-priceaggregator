//
//  ShopCell.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 06/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "ShopCell.h"
@interface ShopCell ()

@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property(weak, nonatomic) IBOutlet UIImageView *iconView;

@end
@implementation ShopCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (instancetype)setupCellWithShop:(NSDictionary *)shop
{
    [[self activityIndicator] startAnimating];
    self.label.text = [shop objectForKey:@"shopName"];
    
    if ([[shop valueForKey:@"download"] boolValue] == YES)
    {
        [[self activityIndicator] setHidden:YES];
        [[self iconView] setHidden:NO];
        
        if ([[shop valueForKey:@"validate"] boolValue] == YES)
        {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:255 / 255.0 green:192 / 255.0 blue:0 / 255.0 alpha:1.0];
            [self setBackgroundView:bgColorView];
            [[self iconView] setImage:[UIImage imageNamed:@"check"]];
        }
        else
        {
            [[self iconView] setImage:[UIImage imageNamed:@"cross"]];
        }
    }
    else if ([[shop valueForKey:@"download"] boolValue] == NO)
    {
        [[self activityIndicator] setHidden:NO];
        [self setBackgroundView:nil];
        [[self iconView] setHidden:YES];
    }
    
    return self;
}

@end

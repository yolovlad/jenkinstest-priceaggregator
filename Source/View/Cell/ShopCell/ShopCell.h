//
//  ShopCell.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 06/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopCell : UITableViewCell
@property(weak, nonatomic) IBOutlet UILabel *label;
@property(weak, nonatomic) NSString *shopID;
@property(assign, nonatomic) BOOL colorNeed;

- (instancetype)setupCellWithShop:(NSDictionary *)shop;

@end

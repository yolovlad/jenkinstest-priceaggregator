//
//  FilterCell.m
//  price-aggregator
//
//  Created by Yalovenko on 04.10.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0];
    [self setSelectedBackgroundView:bgColorView];
}

@end

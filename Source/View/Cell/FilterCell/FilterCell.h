//
//  FilterCell.h
//  price-aggregator
//
//  Created by Yalovenko on 04.10.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "FilterEnum.h"
@interface FilterCell : UITableViewCell
@property(weak, nonatomic) IBOutlet UIView *backgroundViews;
@property(weak, nonatomic) IBOutlet UILabel *filterOptionLabel;
@property(assign, nonatomic) NSString *option;
@property(assign, nonatomic) BOOL stateSelected;

@end

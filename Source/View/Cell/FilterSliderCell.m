//
//  filterSliderCell.m
//  price-aggregator
//
//  Created by Yalovenko on 30.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "FilterSliderCell.h"
@interface FilterSliderCell ()

@property(weak, nonatomic) IBOutlet UILabel *lowerLabel;
@property(weak, nonatomic) IBOutlet UILabel *upperLabel;
@property(weak, nonatomic) IBOutlet NMRangeSlider *slider;

@end
@implementation FilterSliderCell
- (IBAction)sliderValueChanged:(NMRangeSlider *)sender
{
    [self updateRangeLabels];
    [[self delegate] rangeDidChangeWithMin:sender.lowerValue Max:sender.upperValue];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)configureSliderWithBordersValueMin:(CGFloat)minimum Max:(CGFloat)maximum CurrentValueMin:(CGFloat)currentMinimum CurrentValueMax:(CGFloat)currentMaximum
{
    [[self slider] setMinimumValue:minimum];
    [[self slider] setMaximumValue:maximum];
    [[self slider] setUpperValue:currentMaximum animated:NO];
    [[self slider] setLowerValue:currentMinimum animated:NO];
    [self updateRangeLabels];
}

- (void)updateRangeLabels
{
    self.lowerLabel.text = [NSString stringWithFormat:@"%0.f ", self.slider.lowerValue];
    self.upperLabel.text = [NSString stringWithFormat:@"- %0.f UAH", self.slider.upperValue];
}

@end

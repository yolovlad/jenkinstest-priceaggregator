//
//  OfferCell.m
//  price-aggregator
//
//  Created by Yalovenko on 03.10.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "OfferCell.h"
@interface OfferCell ()

@property(weak, nonatomic) IBOutlet UILabel *name;
@property(weak, nonatomic) IBOutlet UILabel *price;
@property(weak, nonatomic) IBOutlet UILabel *category;
@property(weak, nonatomic) IBOutlet UILabel *availableStatus;
@property(weak, nonatomic) IBOutlet UILabel *shop;
@property(weak, nonatomic) IBOutlet UIButton *likeButton;
@property(weak, nonatomic) IBOutlet UILabel *notAvailableStatus;

@end
@implementation OfferCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [[self likeButton]  setImage:[UIImage imageNamed:@"heart_white"] forState:UIControlStateNormal];
    [[self likeButton]  setImage:[UIImage imageNamed:@"heart_orange"] forState:UIControlStateSelected];
}

- (IBAction)didSelectLikeButton:(UIButton *)sender
{
    if (sender.isSelected)
    {
        [[self delegate] deleteCellFromFavorite:self];
    }
    else
    {
        [sender setSelected:YES];
        [[self delegate] addCellToFavorite:self];
    }
}

- (IBAction)didSelectBrowserButton:(id)sender
{
    [[self delegate] openURL:[NSURL URLWithString:[self url]]];
}

- (IBAction)didSelectShareButton:(id)sender
{
    [[self delegate] shareCell:self];
}

#pragma self methods
- (instancetype)setupCellWithOffer:(Offer *)offer
{
    [self setUrl:[offer url]];
    [self setPhotoURL:[NSURL URLWithString:[offer imageUrl]]];
    [[self name] setText:[offer name]];
    [[self shop] setText:[offer shopName]];
    [self setShopName:[offer shopName]];
    [[self price] setText:[NSString stringWithFormat:@"%ld UAH", (long)[offer price]]];
    [[self category] setText:[NSString stringWithFormat:@"•%@", [offer category]]];
    [self setIdentifier:[offer offerId]];
    [[self image] pin_setImageFromURL:[NSURL URLWithString:[offer imageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    if ([offer available] == YES)
    {
        [[self availableStatus] setHidden:NO];
        [[self notAvailableStatus] setHidden:YES];
    }
    else
    {
        [[self availableStatus] setHidden:YES];
        [[self notAvailableStatus] setHidden:NO];
    }
    
    if ([offer favorite] == YES)
    {
        [[self likeButton] setSelected:YES];
    }
    else
    {
        [[self likeButton] setSelected:NO];
    }
    
    return self;
}

@end

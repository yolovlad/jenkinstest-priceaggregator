//
//  OfferCell.h
//  price-aggregator
//
//  Created by Yalovenko on 03.10.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import <PINImageView+PINRemoteImage.h>
#import "Offer.h"
#import "OfferActionProtocol.h"
#import <UIKit/UIKit.h>

@interface OfferCell : UICollectionViewCell

@property(weak, nonatomic) id<OfferActionProtocol> delegate;
@property(strong, nonatomic) NSString *identifier;
@property(strong, nonatomic) NSString *shopName;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSURL *photoURL;
@property(weak, nonatomic) IBOutlet UIImageView *image;

- (instancetype)setupCellWithOffer:(Offer *)offer;

@end


typedef enum SortByPrice
{
    sortByPriceAscending,
    sortByPriceDescending,
}SortByPrice;

typedef enum Availability
{
    availabilityAll,
    availabilityOnlyAvailabile,
}Availability;

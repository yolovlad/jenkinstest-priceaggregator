//
//  Storage.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 06/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Storage : NSObject
+ (Storage *)sharedStorage;

@property(strong, nonatomic) NSMutableDictionary *storageDictionary;

@end

//
//  Storage.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 06/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "Storage.h"

@implementation Storage

+ (Storage *)sharedStorage
{
    static Storage *sharedSingleton;
    
    if (!sharedSingleton)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^
        {
            sharedSingleton = [Storage new];
            [sharedSingleton setStorageDictionary:[NSMutableDictionary new]];
        });
    }
    
    return sharedSingleton;
}

@end

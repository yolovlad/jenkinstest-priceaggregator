//
//  Offer.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 06/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Offer : NSObject
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *offerId;
@property(assign, nonatomic) NSUInteger price;
@property(strong, nonatomic) NSString *category;
@property(strong, nonatomic) NSString *imageUrl;
@property(assign, nonatomic) BOOL available;
@property(assign, nonatomic) BOOL favorite;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *shopName;

@end

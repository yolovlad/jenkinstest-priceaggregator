//
//  Filter.h
//  price-aggregator
//
//  Created by Vlad Yalovenko on 05/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//
#import "FilterEnum.h"
#import <Foundation/Foundation.h>

@interface Filter : NSObject

+ (Filter *)sharedFilter;

@property(assign, nonatomic) SortByPrice sortByPrice;
@property(assign, nonatomic) Availability availability;

@property(assign, nonatomic) NSInteger minimumRange;
@property(assign, nonatomic) NSInteger maximumRange;

@property(strong, nonatomic) NSMutableArray *shops;

@end

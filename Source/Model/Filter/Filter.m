//
//  Filter.m
//  price-aggregator
//
//  Created by Vlad Yalovenko on 05/10/2016.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "Filter.h"

@implementation Filter

+ (Filter *)sharedFilter
{
    static Filter *sharedSingleton;
    
    if (!sharedSingleton)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^
        {
            sharedSingleton = [Filter new];
            [sharedSingleton setShops:[NSMutableArray new]];
        });
    }
    
    return sharedSingleton;
}

@end

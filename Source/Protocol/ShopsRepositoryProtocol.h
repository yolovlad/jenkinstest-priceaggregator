#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Filter.h"
@protocol ShopsRepositoryProtocol<NSObject>

@required
- (void)addShopToStorage:(NSString *)shopURL :(void (^)(NSArray *))block;
- (void)deleteShopFromStorage:(NSString *)shopName;

- (void)addFavoriteItem:(NSString *)offerId ToShop:(NSString *)shopName;
- (void)deleteFavoriteItem:(NSString *)offerId FromShop:(NSString *)shopName;

- (NSArray *)getShopList;

@end

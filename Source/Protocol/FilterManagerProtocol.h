#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FilterEnum.h"
#import "Filter.h"
@protocol FilterManagerProtocol<NSObject>

@required
- (void)setOption:(NSString *)option;
- (void)setRangeWithMin:(NSInteger)minimum Max:(NSInteger)maximum;
- (void)setShop:(NSString *)shopName Enable:(BOOL)enable;

- (Filter *)getFilters;
- (void)resetFilterToDefaultSettings;

@end

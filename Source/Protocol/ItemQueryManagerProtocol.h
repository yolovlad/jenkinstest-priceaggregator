#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Filter.h"
@protocol ItemQueryManagerProtocol<NSObject>

@required
- (void)getFavoriteItemList:(void (^)(NSArray *))block;
- (void)getItemListForRequest:(NSString *)query :(void (^)(NSArray *))block;
- (void)getInformationForFilters:(void (^)(NSArray *shopList, NSInteger minimum, NSInteger maximum))block;
@end

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FilterEnum.h"
@protocol ShopQueryManagerProtocol<NSObject>

@required
- (void)addShop:(NSString *)shopURL :(void (^)(NSArray *))block;
- (void)deleteShop:(NSString *)shopName;

- (NSArray *)getShopList;

- (void)addFavoriteItem:(NSString *)offerId ToShop:(NSString *)shopName;
- (void)deleteFavoriteItem:(NSString *)offerId FromShop:(NSString *)shopName;

@end

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NotificationEnum.h"
@protocol SignatureNotificationProtocol<NSObject>

@optional
- (void)makeNewRequest;
- (void)updateFilters;

@end

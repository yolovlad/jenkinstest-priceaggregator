#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol ShopsControllerProtocol<NSObject>

@required
- (NSArray *)getShopList;

- (void)addShop:(NSString *)shopURL :(void (^)(NSArray *))block;
- (void)deleteShop:(NSString *)shopName;

- (void)addFavoriteItem:(NSString *)offerId ToShop:(NSString *)shopName;
- (void)deleteFavoriteItem:(NSString *)offerId FromShop:(NSString *)shopName;
@end

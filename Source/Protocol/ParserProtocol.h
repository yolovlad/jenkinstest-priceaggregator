#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Filter.h"
@protocol ParserProtocol<NSObject>

@required
- (void)parseFile:(NSString *)ymlPath :(void (^)(NSString *, NSError *, NSArray *))parsingFinished;

@end

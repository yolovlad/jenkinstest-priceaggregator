#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol RangeSliderProtocol<NSObject>

@required
- (void)rangeDidChangeWithMin:(CGFloat)minimun Max:(CGFloat)maximum;

@end

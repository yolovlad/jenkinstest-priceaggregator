#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Filter.h"
@protocol ItemsRepositoryProtocol<NSObject>

@required
- (void)addShopAndItemsToStorage:(NSString *)shopURL :(void (^)(NSString *, NSError *))didAddingShop;
- (void)removeItemsFromStorageForShop:(NSString *)shopURL;

- (void)getAllItems:(void (^)(NSArray *))block;
- (void)getItemsFromStorageForRequest:(NSString *)request WithFilter:(Filter *)filter :(void (^)(NSArray *))block;

@end

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OfferCell.h"
@class OfferCell;
@protocol OfferActionProtocol<NSObject>

@required
- (void)deleteCellFromFavorite:(OfferCell *)cell;
- (void)openURL:(NSURL *)url;
- (void)shareCell:(OfferCell *)cell;

@optional
- (void)addCellToFavorite:(OfferCell *)cell;

@end


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Filter.h"
@protocol ItemsControllerProtocol<NSObject>

- (void)getFavoriteItemList:(void (^)(NSArray *))block;
- (void)getItemListForRequest:(NSString *)request WithFilters:(Filter *)filter :(void (^)(NSArray *))block;
- (void)addShopAndItemsToStorage:(NSString *)shopURL :(void (^)(NSString *, NSError *))didAddingShop;

@end
